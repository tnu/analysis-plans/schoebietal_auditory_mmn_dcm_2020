# ratmpi

Dario Schoebi

This analysis plan concerns the analysis of the 'black hooded rat' 
dataset acquired at the Max Planck Institute for Metabolism research.

The data are epidural (electrophysiological) recordings from four 
electrodes during an auditory oddball mismatch negativity paradigm 
(MMN). For each rat, multiple sessions were recorded manipulating two 
experimental factors: The probability of a deviant tone occurring, and, 
in a subset of the rats, a five-fold pharmacological intervention.

The plan contains a description of the raw data, the pipeline for the 
preprocessing, the classical statistics and the description of the 
modelbased (convolution based DCM) analysis.